import AuthorPage from './components/author-page.js';
import Group from './components/group.js';
Vue.use(VueObserveVisibility);
Vue.use(VueMarkdown);

const app = new Vue({
  el: '#app',
  data: {
    content: [],
    votes: {},
    groups: [],
    search: {
      query: '',
      showAuthorOnly: false,
      categories: [
        { name: 'skin', enabled: true },
        { name: 'sfx', enabled: true },
        { name: 'unknown', enabled: true }
      ],
      tags: []
    },
    searchDebounce: null,
    selected: null,
    sort: 'author'
  },
  async mounted() {
    this.content = await fetch('data/data.json').then(res => res.json());
    this.votes = await fetch('data/votes.json').then(res => res.json()).catch(ex => ({}));

    let tags = new Set();
    for (let content of this.content)
      for (let tag of content.tags)
        tags.add(tag);
    this.search.tags = [...tags].map(tag => ({ tag, enabled: false }));

    window.addEventListener('hashchange', (evt) => {
      evt.preventDefault();
      this.hashChange();
    });

    document.addEventListener('keydown', ({ key }) => {
      if (key == 'Escape')
        this.setHash(window.location.hash.split('/').slice(0, -1).join('/'));
    });

    this.hashChange();
  },
  components: { Group },
  watch: {
    search: {
      deep: true,
      handler() {
        clearTimeout(this.searchDebounce);
        this.searchDebounce = setTimeout(() => this.recreateGroups(), 500);
      }
    },
    content: {
      deep: true,
      handler() {
        this.recreateGroups();
      }
    },
    authorGroup(_new, _old) {
      if (_new == _old) return;
      this.recreateGroups();
    }
  },
  computed: {
    authorGroup() {
      let res = /^author-([^\/]+)/.exec(this.selected);
      if (res) return res[1];
      return null;
    },
    variants() {
      let content = {};

      for (let entry of this.content) {
        content[entry.id] = {
          ...entry,
          derivative: false,
          variants: [],
          derivates: []
        }
      }

      for (let entry of this.content) {
        if (!entry.parent) continue;
        if (!content[entry.parent]) {
          console.warn(`Unable to find parent "${entry.parent}" for "${entry.id}"`);
          continue;
        }
        if (content[entry.parent].author != entry.author) {
          content[entry.id].derivative = true;
          content[entry.parent].derivates.push(entry);
        } else {
          content[entry.parent].variants.push(entry);
        }
      }

      return content;
    },
    selectedContent() {
      if (!this.selected) return null;
      let last = this.selected.split('/').slice(-1)[0];
      if (last.startsWith('author-')) return null;
      return last;
    },
    allCollapsed() {
      return this.groups.every(group => group.collapsed);
    }
  },
  methods: {
    resetSearch() {
      this.search.query = '';
      for (let tag of this.search.tags)
        tag.enabled = false;
    },
    recreateGroups() {
      let finalGroups = [];
      if (this.authorGroup) {
        this.sort = null;
        finalGroups.push({
          name: this.authorGroup,
          component: AuthorPage,
          prefix: `author-${this.authorGroup}`,
          collapsible: false,
          collapsed: false,
          content: this.content
            .map(content => this.variants[content.id])
            .filter(content => {
              if (content.author != this.authorGroup) return false;
              return content.parent == null || content.derivative;
            })
        });
      } else {
        let groups = {};
        if (this.sort == null)
          this.sort = 'author';
        let excluded = {};

        this.content
          .filter(el => {
            let fail = () => {
              if (this.sort == 'author')
                excluded[el.author] = (excluded[el.author] ?? 0) + 1;
              return false;
            };

            if (!this.search.showAuthorOnly)
              if (el.visibility == 'author' || el.derivative)
                return fail();

            for (let { name, enabled } of this.search.categories)
              if (el.type == name && !enabled) return fail();

            for (let { tag, enabled } of this.search.tags) {
              if (!enabled) continue;
              if (!el.tags.includes(tag)) return fail();
            }

            if (this.search.query.trim().length > 0) {
              let lower = this.search.query.toLowerCase().trim();
              let name = el.name.toLowerCase().includes(lower);
              let author = el.author.toLowerCase().includes(lower);
              if (!name && !author) return fail();
            }

            return true;
          })
          .map(el => {
            switch (this.sort) {
              case 'author':
                if (el.parent && !el.derivative) return null;
                return this.variants[el.id];

              case 'votes':
                if (!this.votes[el.id]?.active)
                  return null;
                return { ...el, variants: [], derivates: [] };

              case 'votes-all':
                if (!this.votes[el.id]?.total)
                  return null;
                return { ...el, variants: [], derivates: [] };

              case 'date':
                return { ...el, variants: [], derivates: [] };
            }
          })
          .filter(el => el)
          .sort((a,b) => {
            let ord = (a, b) => a > b ? 1 : a == b ? 0 : -1;
            switch (this.sort) {
              case 'author': return ord(a.author, b.author);
              case 'votes': return -ord(
                this.votes[a.id]?.active || 0,
                this.votes[b.id]?.active || 0
              );
              case 'votes-all': return -ord(
                this.votes[a.id]?.total || 0,
                this.votes[b.id]?.total || 0
              );
              case 'date': return -ord(a.uploaded || 0, b.uploaded || 0);
            }
          })
          .forEach(content => {
            let getGroup = id => {
              if (!groups[id]) {
                groups[id] = [];
                groups[id].excluded = excluded[id] ?? 0;
              }
              return groups[id];
            }
            switch (this.sort) {
              case 'author': getGroup(content.author).push(content); break;
              case 'votes': getGroup('Skins with active votes').push(content); break;
              case 'votes-all': getGroup('Skins with votes').push(content); break;
              case 'date': getGroup('Recent skins').push(content); break;
            }
          });

        finalGroups.push(...Object.entries(groups).map(([name, content]) => {
          return {
            name: name,
            prefix: ``,
            collapsible: Object.entries(groups).length > 1,
            collapsed: false,
            content: content
          }
        }));
      }
      let existingCollapsed = Object.fromEntries(this.groups.map(el => {
        return [el.name, el.collapsed]
      }));
      for (let group of finalGroups)
        if (existingCollapsed[group.name])
          group.collapsed = true;
      this.groups = finalGroups;
    },
    setSort(sort) {
      this.sort = sort;
      window.location.hash = `#${this.selectedContent}`;
      this.recreateGroups();
      this.scrollToSelected();
    },
    scrollToSelected() {
      this.$nextTick(() => {
        let el = document.getElementById(this.selectedContent);
        if (!el) {
          window.scrollTo(0, 0);
          return;
        }
        el.scrollIntoView({
          behavior: 'auto',
          block: 'center',
          inline: 'center'
        });
      });
    },
    collapseAll() {
      for (let group of this.groups)
        group.collapsed = true;
    },
    uncollapseAll() {
      for (let group of this.groups)
        group.collapsed = false;
    },
    randomSkin() {
      window.location.hash = this.content[~~(Math.random() * this.content.length)].id;
    },
    hashChange() {
      let hash = window.location.hash;
      if (!hash) {
        this.selected = null;
        return;
      }
      hash = decodeURIComponent(hash.slice(1));
      this.selected = hash;

      let content = this.content.filter(({ id }) => id == hash)[0];
      if (!this.authorGroup && (content?.visibility == 'author' || content.derivative))
        this.setHash(`author-${content.author}/${content.id}`);

      if (content)
        this.scrollToSelected();
    },
    setHash(hash) {
      let url = new URL(window.location);
      url.hash = hash;
      console.log("redirect", url.toString());
      history.replaceState(null, '', url);
      this.hashChange();
    }
  }
});

window.app = app;
