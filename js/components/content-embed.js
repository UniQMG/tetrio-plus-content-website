const html = arg => arg.join('');
import skin from './preview/skin.js';
import sfx from './preview/sfx.js';
import unknown from './preview/unknown.js';
import custom from './preview/custom.js';

export default {
  name: 'content-embed',
  template: html`
    <div :id="activeContent.id" class="content-embed" :class="clazz">
      <div class="content-title">
        <span v-if="derivative" class="derivative" title="Derivative work">⮩</span>
        <span class="overflowable-name-wrapper" :title="activeContent.name">
          <a class="content-name no-display" :href="contentHref">{{ activeContent.name }}</a>
        </span>
        <span class="variants" v-if="hasVariants">
          <span v-if="variantIndex == 0 && !isSelected">
            &nbsp;+&nbsp;{{ content.variants.length }}&nbsp;<!--
            -->variant{{ content.variants.length > 1 ? 's' : '' }}
          </span>
          <span v-else>
            #{{ variantIndex+1 }} of {{ content.variants.length+1 }}
          </span>
        </span>
        <span class="new" v-if="recent">new</span>

        <span class="flex-spacer"/>

        <span class="votes active" v-if="activeVotes" title="Votes in the past week">
          +{{ activeVotes }}
        </span>
        <span
          v-if="supportsPreview"
          class="supports-preview"
          :title="
            'This resource successfully imports automatically ' +
            'and supports instant preview'
           "
        >✓</span>
        <button class="download-button" v-if="showTpse" title="Download a TPSE, for use in TETR.IO PLUS or Microplus Toolkit for TETR.IO" style="opacity: 0.4;">
          <a :href="'data/' + activeContent.tpsePath" download>TPSE ⭳</a>
        </button>
        <button class="download-button" title="Download the original content files, for remixing or usage in TETR.IO PLUS">
          <a :href="'data/' + activeContent.path" download>{{ extension.toUpperCase() }} ⭳</a>
        </button>
      </div>

      <component
        v-if="preview"
        :is="preview"
        :content="activeContent"
        :selected="isSelected"
      >
        <template v-slot:badges>
          <span class="content-badge neutral" v-for="tag of activeContent.tags">
            {{ tag }}
          </span>
        </template>
      </component>

      <template v-if="isSelected">
        <div class="content-description">
          <b v-if="derivative">
            Derivative work of
            <a :href="'#' + activeContent.parent">{{activeContent.parent}}</a>
          </b>
          <div v-if="activeContent.description">
            <vue-markdown
              :source="activeContent.description"
              :html="false"
              :linkify="false"
              :anchorAttributes="{ rel: 'nofollow' }"
            />
          </div>
          <div v-else>No description</div>
          <div>
            Author:
            <b>
              <a v-if="authorPage" :href="authorPage">{{ activeContent.author }}</a>
              <template v-else>{{ activeContent.author }}</template>
            </b>
          </div>
          <div v-if="activeContent.uploaded">
            Uploaded: <time :datetime="uploadDateISO">{{ uploadDateShort }}</time>
          </div>
          <div v-if="activeContent.updated != activeContent.uploaded">
            Updated: <time :datetime="updateDateISO">{{ updateDateShort }}</time>
          </div>
          <div>
            Votes:
            <span :class="activeVotes > 0 && 'votes'"><b>+{{ activeVotes }}</b></span>
            (total: <b>{{ totalVotes }}</b>)
          </div>
        </div>

        <div class="content-controls">
          <button @click="open('firefox')" :disabled="!supportsPreview">
            Open in Firefox
          </button>
          <button @click="open('desktop')" :disabled="!supportsPreview">
            Open in Desktop
          </button>
          <button @click="vote()" :class="voteResult" title="Vote for this submission">+1</button>
          <button @click="lastVariant()" :disabled="!hasVariants">🡄</button>
          <button @click="nextVariant()" :disabled="!hasVariants">🡆</button>
        </div>
      </template>
    </div>
  `,
  props: ['content', 'prefix', 'votes', 'selected'],
  data: () => ({
    previews: { sfx, skin },
    variantIndex: 0,
    voteResult: null
  }),
  watch: {
    selected(selected) {
      [this.content, ...this.content.variants].forEach((item, i) => {
        if (selected == item.id) this.variantIndex = i;
      });
    }
  },
  computed: {
    derivative() {
      return this.activeContent.derivative;
    },
    authorPage() {
      let page = '#author-' + this.activeContent.author;
      if (window.location.hash.startsWith(page))
        return null;
      return page;
    },
    contentHref() {
      let components = [];
      if (this.prefix) components.push(this.prefix);
      components.push(this.activeContent.id);
      return '#' + components.join('/');
    },
    clazz() {
      return {
        'is-selected': this.isSelected
      }
    },
    activeVotes() {
      return this.votes[this.activeContent.id]?.active || 0;
    },
    totalVotes() {
      return this.votes[this.activeContent.id]?.total || 0;
    },
    showTpse() {
      if (!this.activeContent.tpsePath) return false;
      //if (this.activeContent.type == 'skin') return false;
      if (this.extension == 'tpse') return false;
      return true;
    },
    isSelected() {
      return this.activeContent.id == this.selected;
    },
    hasVariants() {
      return this.content.variants.length > 0
    },
    supportsPreview() {
      return !!this.activeContent.tpsePath;
    },
    activeContent() {
      if (this.variantIndex == 0) return this.content;
      return this.content.variants[this.variantIndex-1];
    },
    preview() {
      if (this.activeContent.previewPath)
        return custom;
      switch (this.activeContent.type) {
        case 'skin': return skin;
        case 'sfx': return sfx;
        default: return unknown;
      }
    },
    extension() {
      return this.activeContent.path.split('.').slice(-1)[0];
    },
    uploadDateISO() {
      return new Date(this.activeContent.uploaded).toISOString();
    },
    updateDateISO() {
      return new Date(this.activeContent.updated).toISOString();
    },
    uploadDateShort() { return this.uploadDateISO.split('T')[0] },
    updateDateShort() { return this.updateDateISO.split('T')[0] },
    recent() {
      const ONE_WEEK = 7 * 24 * 60 * 60 * 1000;
      return (Date.now() - this.activeContent.uploaded) < ONE_WEEK;
    }
  },
  methods: {
    async vote(retried=false) {
      let endpoint = window.location.hostname.startsWith('localhost')
        ? 'http://localhost:8081/api/'
        : '/tetrioplus/api/';
      let token = localStorage.voteToken;
      let res = await fetch(
        `${endpoint}vote/${this.activeContent.id}?token=${token}`,
        { method: 'POST' }
      );
      switch (res.status) {
        case 400: // bad token
          let err = await res.text();
          if (err == 'Token not found') {
            if (retried) {
              alert('Invalid token');
              return;
            }
            let discord = document.getElementById('discord').href;
            let token = prompt(
              "To vote, you must link your discord account. In the TETR.IO PLUS " +
              "discord, #bots channel, use the `rv!link` command. Paste the " +
              "token that the bot gives you here.\n\nDiscord link: " + discord
            );
            if (token == null) return; // Didn't provide a token
            localStorage.voteToken = token;
            await this.vote(true);
          } else if (err == 'Already voted today') {
            this.voteResult = ['floating-vote', 'vote-failed'];
            setTimeout(() => this.voteResult = null, 2000);
          } else {
            alert(err);
          }
          break;
        case 200:
          if (!this.votes[this.activeContent.id])
            this.votes[this.activeContent.id] = { active: 0, total: 0 };
          this.votes[this.activeContent.id].active += 1;
          this.votes[this.activeContent.id].total += 1;
          this.voteResult = ['floating-vote', 'vote-success'];
          setTimeout(() => this.voteResult = null, 2000);
          break;
        case 404: throw new Error('Vote: content not found');
        default: throw new Error('Unexpected response code ' + res.status);
      }
    },
    lastVariant() {
      this.variantIndex = this.variantIndex == 0
        ? this.content.variants.length
        : this.variantIndex - 1;
      window.location.hash = this.contentHref;
    },
    nextVariant() {
      this.variantIndex++;
      if (this.variantIndex > this.content.variants.length)
        this.variantIndex = 0;
      window.location.hash = this.contentHref;
    },
    select() {
      window.location.hash = this.content.id;
    },
    open(platform) {
      let tpsepath = this.activeContent.tpsePath;
      let loc = location.protocol + "//" + location.host + location.pathname;
      let tpse = encodeURIComponent(loc.replace(/\/$/, '') + '/data/' + tpsepath);

      if (platform == 'firefox')
        window.open('https://tetr.io/?useContentPack=' + tpse);

      if (platform == 'desktop')
        window.open('tetrio://tetrioplus/tpse/' + tpse);
    }
  }
}
