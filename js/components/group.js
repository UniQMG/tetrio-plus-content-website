import ContentEmbed from './content-embed.js';

const html = arg => arg.join('');
export default {
  template: html`
    <div class="group">
      <div class="group-header" @click="toggleCollapse">
        <span class="toggle" v-if="group.collapsible">
          {{ group.collapsed ? '+' : '-' }}
        </span>
        {{ group.name }}
        <span class="skin-count">
          {{ group.content.length }}
          <span
            class="removed"
            v-if="group.content.excluded > 0"
            title="Number of items excluded by search options"
          >
            ({{ group.content.excluded }})
          </span>
        </span>
      </div>
      <component
        :is="group.component"
        v-if="group.component"
        :group="group"
        :votes="votes"
        :selected="selected"
      />
      <content-embed
        v-for="content of group.content"
        v-show="!group.collapsed"
        :key="content.id"
        :prefix="group.prefix"
        :content="content"
        :votes="votes"
        :selected="selected"
      />
    </div>
  `,
  props: ['group', 'votes', 'selected'],
  components: { ContentEmbed },
  methods: {
    toggleCollapse() {
      if (!this.group.collapsible) return;
      this.group.collapsed = !this.group.collapsed;
    }
  }
}
