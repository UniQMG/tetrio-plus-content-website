const html = arg => arg.join('');
export default {
  template: html`
    <div class="content-preview custom" :class="{ 'is-selected': selected }">
      <video
        v-if="video"
        class="custom-preview"
        :src="'data/' + path"
        controls
        @play="played"
        preload="none"
      />
      <img
        v-else
        class="custom-preview"
        :src="'data/' + path"
        loading="lazy"
      />
      <div v-if="selected">
        ⚠️ User media, not an in-game preview.
      </div>
      <div class="badges">
        <slot name="badges"></slot>
      </div>
    </div>
  `,
  props: ['content', 'selected'],
  computed: {
    path() {
      return this.content.previewPath;
    },
    video() {
      let formats = ['mp4', 'webm', 'mov', 'avi', 'ogv'];
      let ext = this.content.previewPath.split('.').slice(-1)[0];
      return formats.includes(ext);
    }
  },
  methods: {
    played() {
      window.location.hash = this.content.id;
    }
  }
}
