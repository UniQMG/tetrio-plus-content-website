import SkinSplicer from '../../lib/skin-splicer.js'
const html = arg => arg.join('');

const g = 'ghost';
const gb = 'gb';
const PCO_MAP = [
  [['z', 0b00100], ['z', 0b10011], ['_', 0b00000], ['_', 0b00000], ['_', 0b00000], ['_', 0b00000], ['i', 0b00100], ['i', 0b00101], ['i', 0b00101], ['i', 0b00001]],
  [['t', 0b00010], ['z', 0b11100], ['z', 0b00001], ['_', 0b00000], ['_', 0b00000], [ g , 0b00010], ['l', 0b00010], ['o', 0b00110], ['o', 0b00011], ['j', 0b00010]],
  [['t', 0b11110], ['t', 0b00001], ['s', 0b10110], ['s', 0b00001], [ g , 0b00100], [ g , 0b11011], ['l', 0b01010], ['o', 0b01100], ['o', 0b01001], ['j', 0b01010]],
  [['t', 0b01000], ['s', 0b00100], ['s', 0b11001], ['_', 0b00000], ['_', 0b00000], [ g , 0b01000], ['l', 0b11100], ['l', 0b00001], ['j', 0b00100], ['j', 0b11001]],
  [[ gb, 0b00100], [ gb, 0b00101], [ gb, 0b00101], [ gb, 0b00001], ['_', 0b00000], [ gb, 0b00100], [ gb, 0b00101], [ gb, 0b00101], [ gb, 0b00101], [ gb, 0b00001]],
];
const PREVIEW_MAP = [[
  ['z', 0], ['l', 0], ['o', 0], ['s', 0], ['i', 0], ['j', 0], ['t', 0],
  ['ghost', 0], ['hold', 0], ['gb', 0], ['dgb', 0], ['topout', 0],
]];

export default {
  template: html`
    <div class="content-preview skin">
      <div class="skin-preview-container">
        <img
          class="skin-preview"
          :src="imagehref"
          width="372"
          height="30"
          v-if="simple"
        />
        <canvas
          v-else
          width="372"
          height="30"
          ref="canvas"
          class="skin-preview"
          v-observe-visibility="{ callback: visibilityChanged, throttle: 300 }"
          @click="$emit('select')"
        />
      </div>
      <div v-if="!simple && selected">
        ⚠️ Actual in-game appearance may differ.
      </div>
      <div class="badges">
        <span class="content-badge" v-if="connected">Connected</span>
        <!-- <span class="content-badge" v-if="animated">Animated</span> -->
        <span class="content-badge bad" v-if="hasMinos && !hasGhost">Minos only</span>
        <span class="content-badge bad" v-if="hasGhost && !hasMinos">Ghost only</span>
        <slot name="badges"></slot>
      </div>
      <!-- <div class="skin-preview-options" v-if="connected">
        <input v-model.boolean="connection[0]" name="corner" type="checkbox" />
        <label for="corner">Corner</label>
        <input v-model.boolean="connection[1]" name="n" type="checkbox" />
        <label for="n">N</label>
        <input v-model.boolean="connection[2]" name="e" type="checkbox" />
        <label for="e">E</label>
        <input v-model.boolean="connection[3]" name="s" type="checkbox" />
        <label for="s">S</label>
        <input v-model.boolean="connection[4]" name="w" type="checkbox" />
        <label for="w">W</label>
      </div> -->
    </div>
  `,
  props: ['content', 'selected'],
  data: () => ({
    connection: [false, false, false, false, false],
    hasMinos: true,
    hasGhost: true,
    fetched: false,
    visible: false,
    images: []
  }),
  watch: {
    visible(val) {
      if (val) this.fetchImage().then(() => this.redraw())
    },
    selected() {
      this.$nextTick(() => {
        this.fetchImage().then(() => this.redraw());
      });
    },
    async content() {
      this.images.length = 0;
      this.fetched = false;
      await this.fetchImage();
    },
    connection: {
      deep: true,
      handler() {
        this.redraw();
      }
    }
  },
  computed: {
    simple() {
      if (this.selected) return false;
      return [
        'tetriosvg', 'tetrioraster', 'tetrioanim',
        'jstrisraster', 'jstrisanim'
      ].indexOf(this.content.format) != -1;
    },
    connected() {
      return this.content.format.indexOf('connected') != -1;
    },
    animated() {
      return this.content.format.indexOf('anim') != -1;
    },
    imagehref() {
      return 'data/' + this.content.path;
    }
  },
  methods: {
    async fetchImage() {
      if (this.fetched || this.simple) return;
      this.fetched = true;

      for (let img of this.images)
        URL.revokeObjectURL(img.src);

      let images = [];
      if (this.imagehref.split('.').slice(-1) == 'zip') {
        let blob = await (await fetch(this.imagehref)).blob();
        let zip = await JSZip.loadAsync(blob);

        // Some uploads have been zip files with a single nested zip file inside containing the skin.
        // This previewer will never be as robust as tpsecore, but in the meantime here's some
        // hacked-together support to get those uploads previewing correctly.
        while (Object.keys(zip.files)[0]?.endsWith('.zip')) {
          let innerZip = Object.values(zip.files)[0].async('blob');
          zip = await JSZip.loadAsync(innerZip);
        }

        let files = Object.entries(zip.files).filter(([_, file]) => {
          if (file.dir) return false;
          if (file.name.includes("__MACOSX")) return false;
          if (file.name.includes(".DS_Store")) return false;
          return true;
        }).sort(([k1, v1], [k2, v2]) => {
          let pri1 = k1.indexOf('ghost') !== -1 ? 0 : 1;
          let pri2 = k2.indexOf('ghost') !== -1 ? 0 : 1;
          return pri1 > pri2 ? -1 : 1;
        });
        for (let [key, file] of files) {
          let img = new Image();
          let blob = await file.async('blob');
          if (file.name.endsWith('svg')) {
            // Doesn't render SVGs correctly without content type set
            blob = blob.slice(0, blob.size, 'image/svg+xml');
          }
          await new Promise((res, rej) => {
            img.onload = res;
            img.onerror = rej;
            img.src = URL.createObjectURL(blob);
          });
          images.push(img);
        }
      } else {
        let img = new Image();
        img.src = this.imagehref;
        await new Promise(res => img.onload = res);
        images.push(img);
      }

      this.images = images;
      this.redraw();
    },
    visibilityChanged(visible) {
      this.visible = visible;
    },
    redraw() {
      if (!this.content.format || this.simple) return;
      let canvas = this.$refs.canvas;
      if (!canvas || !this.images.length) return;
      console.log('redrawing', this.content.id);
      let ctx = canvas.getContext('2d');
      ctx.imageSmoothingEnabled = false;

      let map = this.selected ? PCO_MAP : PREVIEW_MAP;

      let splicer = new SkinSplicer(this.content.format, this.images);
      this.hasMinos = !!splicer.get('z');
      this.hasGhost = !!splicer.get('ghost');

      let { width, height } = canvas.getBoundingClientRect();
      height = Math.floor(width * map.length/map[0].length);
      width = Math.floor(width);
      canvas.width = width;
      canvas.height = height;

      for (let y = 0; y < map.length; y++) {
        for (let x = 0; x < map[y].length; x++) {
            let [piece, conn] = map[y][x];
            let tex = splicer.get(piece, conn);
            if (!tex) continue;

            ctx.globalAlpha = this.selected && piece == 'ghost' ? 0.25 : 1;
            let padding = this.content.format.startsWith('tetrio61')
              ? this.images[0].width >= 2048 ? 2 : 1
              : 0;
            tex[1] = Math.floor(tex[1] + padding);
            tex[2] = Math.floor(tex[2] + padding);
            tex[3] = Math.floor(tex[3] - padding*2);
            tex[4] = Math.floor(tex[4] - padding*2);

            let dest = [
              x * width/map[y].length,
              y * height/map.length,
              width / map[y].length+1,
              height / map.length+1
            ].map(coord => Math.floor(coord));
            ctx.clearRect(...dest);
            ctx.drawImage(...tex, ...dest);
        }
      }
    }
  }
}
