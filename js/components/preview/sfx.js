const html = arg => arg.join('');

export default {
  template: html`
    <div class="content-preview sfx">
      <template v-if="isTPSE">
        No preview available for this sound effect pack because it was uploaded as a TPSE file.
      </template>
      <template class="sfx-preview" v-else-if="!selected">
        Select to preview {{ content.modified.length || '' }} changed sounds.
      </template>
      <template class="sfx-preview" v-else-if="error">
        Failed to load preview.
      </template>
      <template class="sfx-preview" v-else-if="!files">
        Loading...
      </template>
      <template v-else>
        <div v-for="file of files" :key="file.name">
          <button @click="play(file)">▶</button> {{ file.name }}
        </div>
      </template>
      <div class="badges">
        <slot name="badges"></slot>
      </div>
    </div>
  `,
  props: ['content', 'selected'],
  data: () => ({
    loadedfiles: null,
    error: null
  }),
  computed: {
    isTPSE() {
      return this.content.format == 'tpse';
    },
    files() {
      if (!this.loadedfiles) this.load();
      return this.loadedfiles;
    },
    path() {
      return 'data/' + this.content.path;
    }
  },
  methods: {
    async load() {
      if (this.isTPSE) return;
      try {
        let promise = (await fetch(this.path)).blob();
        let zip = await JSZip.loadAsync(await promise);
        let files = [];
        for (let [key, file] of Object.entries(zip.files))
          files.push({ name: key, blob: await file.async('blob') });
        this.loadedfiles = await Promise.all(files);
      } catch(ex) {
        console.error(ex);
        this.error = ex;
      }
    },
    play(file) {
      let audio = new Audio();
      audio.src = URL.createObjectURL(file.blob);
      audio.onended = () => URL.revokeObjectURL(audio.src);
      audio.play();
    }
  }
}
