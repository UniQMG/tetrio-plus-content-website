const html = arg => arg.join('');
export default {
  template: html`
    <div class="content-preview unknown">
      No preview available
      <div class="badges">
        <slot name="badges"></slot>
      </div>
    </div>
  `
}
