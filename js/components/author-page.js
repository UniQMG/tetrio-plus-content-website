const html = arg => arg.join('');
export default {
  template: html`
    <div class="author-page">
      All content by {{ group.name }}
    </div>
  `,
  props: ['group']
}
